using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarController : MonoBehaviour
{
    public void SetSize(float sizeNormalized)
    {
        transform.localScale = new Vector3(sizeNormalized, 1f, 3f);
    }
}
