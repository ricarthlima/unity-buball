using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    Rigidbody2D rb;
    Vector3 lastVelocity;

    public float maxVelocity;
    public float decelerationRate;
    public float loseEnergyRate;
    public float shootStrength;

    bool isTouchingWall;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        lastVelocity = rb.velocity;
        Decelerate();
    }

    /**
     * https://www.youtube.com/watch?v=RoZG5RARGF0 
     */

    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "Wall")
        {
            isTouchingWall = true;

            var speed = lastVelocity.magnitude;
            var direction = Vector3.Reflect(lastVelocity.normalized, col.contacts[0].normal);

            rb.velocity = direction * Mathf.Max(speed * (1 - (loseEnergyRate * 0.1f)), 0);
        }

        if (col.gameObject.CompareTag("LeftGoal"))
        {
            Debug.Log("LEFT GOAL!");
        }

        if (col.gameObject.CompareTag("RightGoal"))
        {
            Debug.Log("RIGHT GOAL!");
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            isTouchingWall = false;
        }
    }

    void Decelerate()
    {
        rb.velocity -= decelerationRate * rb.velocity * Time.deltaTime;
    }

    public bool Shoot(Vector3 direction)
    {

        rb.AddForce(direction * shootStrength * 100 * (isTouchingWall ? -1 : 1));
        return isTouchingWall;
    }
}
