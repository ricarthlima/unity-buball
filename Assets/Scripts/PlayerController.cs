using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float maxVelocity;
    public float accelerationRate;
    public float decelerationRate;
    public float whenPushDecelarationRate;

    [Header("Dash Properties")]
    public float dashRate;
    public float dashCount;
    public int dashMax;

    Rigidbody2D rb;
    bool isShooting;
    BarController dashBarController;

    #region "Life Cycles"
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        dashBarController = transform.Find("DashBar").gameObject.transform.Find("Bar").gameObject.GetComponent<BarController>();
    }

    // Update is called once per frame
    void Update()
    {
        KeyController();
        ReloadDash();
    }


    void FixedUpdate()
    {
        rb.velocity = Vector2.ClampMagnitude(rb.velocity, maxVelocity);
    }

    #endregion

    #region "Controllers"
    void KeyController()
    {
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                rb.AddForce(Vector2.up * accelerationRate);
            }

            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb.AddForce(Vector2.down * accelerationRate);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.AddForce(Vector2.left * accelerationRate);
            }

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.AddForce(Vector2.right * accelerationRate);
            }
        }
        else
        {
            rb.velocity -= (decelerationRate * (Mathf.Pow(10, -2))) * rb.velocity;
        }

        if (Input.GetKeyDown(KeyCode.X))
        {
            GetComponent<SpriteRenderer>().color = Color.black;
            maxVelocity = maxVelocity / whenPushDecelarationRate;
            isShooting = true;
        }

        if (Input.GetKeyUp(KeyCode.X))
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            maxVelocity = maxVelocity * whenPushDecelarationRate;
            isShooting = false;
        }

        if (Input.GetKeyDown(KeyCode.C))
        {
            Dash();

        }
    }

    #endregion

    #region "Dash"
    private void Dash()
    {
        if (dashCount < dashMax - 1)
        {
            dashCount++;
            dashCount = Mathf.Min(dashCount, dashMax);
            StartCoroutine("DoDash");
        }
    }

    private IEnumerator DoDash()
    {
        accelerationRate *= dashRate;
        yield return new WaitForSeconds(0.5f);
        accelerationRate /= dashRate;
    }

    private void ReloadDash()
    {
        dashBarController.SetSize(dashCount / dashMax);
        if (dashCount > 0)
        {
            dashCount -= 0.1f * Time.deltaTime;
        }
        else
        {
            dashCount = 0;
        }
    }
    #endregion

    #region "Collisions"
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
        {
            if (isShooting)
            {
                Vector3 direction = (collision.gameObject.transform.position - transform.position).normalized;
                collision.gameObject.GetComponent<BallController>().Shoot(direction);
            }
        }
    }
    #endregion
}
